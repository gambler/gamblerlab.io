---
title: 你好,世界.
cover: 
    - ../hello-world/1.jpg
categories:
    - 杂烩
tags:
    - introduction
---
> 如果你是第一次来, 请进这里.
<!-- more -->
***
感谢参观[树洞](https://gambler.gitlab.io/hexo/),
一个小动物躲避寒冬的地方.
是无聊时消磨的工具, 是一本提供消遣的故事书.
### 关于内容:
最可能是不定期更新些有趣的话题, 新闻, 短故事一类的. 不过还请不要期望太多, 博主是个很忙(lan)的人. 一旦闲下来就会写一写, 故事话题不限, 只要值得一说的都会写.
### 基于:
博客基于[Hexo](https://hexo.io/), 主题[Aero-Dual](https://github.com/levblanc/hexo-theme-aero-dual), repo挂在[GitLab Pages](https://about.gitlab.com/features/pages/)上.
博主会尝试挂载更多功能. (大概吧)

那么, 多关照了, random strangers from the internet.